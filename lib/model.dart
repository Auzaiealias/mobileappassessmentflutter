class Model {
  Model({
    this.title,
    this.dateStart,
    this.dateEnd,
    this.tick = false,
  });

  final String title;
  final String dateStart;
  final String dateEnd;
  final bool tick;

  Model copyWith({
    String title,
    String dateStart,
    String dateEnd,
    bool tick,
}) => Model(
      title: title ?? this.title,
      dateStart: dateStart ?? this.dateStart,
      dateEnd: dateEnd ?? this.dateEnd,
      tick: tick ?? this.tick,
  );
}
