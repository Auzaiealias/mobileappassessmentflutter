import 'package:assessment/model.dart';
import 'package:assessment/utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rounded_date_picker/rounded_picker.dart';

class AddNewToDoList extends StatefulWidget {
  final Function addValue;

  const AddNewToDoList({Key key, this.addValue}) : super(key: key);

  @override
  _AddNewToDoListState createState() => _AddNewToDoListState();
}

class _AddNewToDoListState extends State<AddNewToDoList> {
  DateTime dateTimeStart, dateTimeEnd;
  final controller = TextEditingController();

  @override
  Widget build(BuildContext context) {
    Future showDatePicker() => showRoundedDatePicker(
            context: context,
            initialDate: DateTime.now(),
            firstDate: DateTime(DateTime.now().year - 1),
            lastDate: DateTime(DateTime.now().year + 1),
            theme: ThemeData(primarySwatch: Colors.amber))
        .then((selectedDate) => DateTime.parse('$selectedDate'));

    return Scaffold(
        appBar: AppBar(
          title:
              Text("Add New To-Do List", style: TextStyle(color: Colors.black)),
          backgroundColor: Color.fromRGBO(250, 186, 44, 1),
        ),
        body: Container(
          padding: const EdgeInsets.all(30),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                'To-Do Title',
                style: TextStyle(
                  fontSize: 12,
                  color: Colors.grey,
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(5),
              ),
              Container(
                child: TextField(
                  controller: controller,
                  decoration: InputDecoration(
                    labelText: '  Please key in your To-Do title here',
                    labelStyle: TextStyle(fontSize: 12),
                    contentPadding: const EdgeInsets.symmetric(vertical: 50),
                    border: new OutlineInputBorder(
                        borderRadius: new BorderRadius.circular(5),
                        borderSide: BorderSide()),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(10),
              ),
              Text(
                'Start Date',
                style: TextStyle(
                  fontSize: 12,
                  color: Colors.grey,
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(5),
              ),
              InkWell(
                onTap: () async {
                  final date = await showDatePicker();

                  setState(() {
                    dateTimeStart = date;
                  });
                },
                child: Container(
                  height: 30,
                  child: Card(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(5),
                    ),
                    child: new Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        new Text(
                          dateTimeStart == null
                              ? 'Start Date'
                              : getFormattedDate('$dateTimeStart'),
                        ),
                        new Icon(Icons.arrow_drop_down,
                            color:
                                Theme.of(context).brightness == Brightness.light
                                    ? Colors.grey.shade700
                                    : Colors.white70),
                      ],
                    ),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(10),
              ),
              Text(
                'Estimate End Date',
                style: TextStyle(
                  fontSize: 12,
                  color: Colors.grey,
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(5),
              ),
              InkWell(
                onTap: () async {
                  final date = await showDatePicker();

                  setState(() {
                    dateTimeEnd = date;
                  });
                },
                child: Container(
                  height: 30,
                  child: Card(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(5),
                    ),
                    child: new Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        new Text(dateTimeEnd == null
                            ? 'End Date'
                            : getFormattedDate('$dateTimeEnd')),
                        new Icon(Icons.arrow_drop_down,
                            color:
                                Theme.of(context).brightness == Brightness.light
                                    ? Colors.grey.shade700
                                    : Colors.white70),
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
        bottomNavigationBar: FlatButton(
          color: Colors.black,
          textColor: Colors.white,
          padding: const EdgeInsets.all(20),
          //splashColor: Colors.black,
          onPressed: () {
            final model = Model(
                dateEnd: dateTimeEnd.toString(),
                dateStart: dateTimeStart.toString(),
                title: controller.value.text);
            widget.addValue(model: model);
            print("testDate: ${model.dateEnd}");
            print("testDate: ${model.dateStart}");
            Navigator.pop(context);
          },
          child: Text(
            'Create Now',
            style: TextStyle(fontSize: 15),
          ),
        ));
  }
}
