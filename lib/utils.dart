import 'package:intl/intl.dart';

String getFormattedDate(String input) {
  final dateFormat = DateFormat('d/MM/yyyy');
  return dateFormat.format(DateTime.parse(input));
}
